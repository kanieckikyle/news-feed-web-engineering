//this forces javascript to conform to some rules, like declaring variables with var
"use strict";
var url = "http://www.espn.com/espn/rss/";
var selectedNewsFeeds = ['NHL', 'MLB', 'NBA'];
var globalItems = [];
var favorites = [];

const { Observable, from, forkJoin } = rxjs;
const { filter, throttleTime } = rxjs.operators;

$(document).ready(function() {
  init();
});

$(window).on('beforeunload', function() {
  saveFavorites();
});

function init() {
  //NHL URL for ESPN RSS feed
  console.log("Entering Init");
  document.querySelector("#content").innerHTML = "<b>Loading news...</b>";

  loadFavorites();

  //fetch the data
  fetchNews();

  $("#MLB").change((event) => {
    var checked = event.target.checked;
    console.log('MLB Changed');
    if (checked) {
      selectedNewsFeeds.push('MLB');
    } else {
      selectedNewsFeeds = selectedNewsFeeds.filter(feed => feed !== 'MLB');
    }
    fetchNews();
  });

  $("#NHL").change((event) => {
    var checked = event.target.checked;
    if (checked) {
      selectedNewsFeeds.push('NHL');
    } else {
      selectedNewsFeeds = selectedNewsFeeds.filter(feed => feed !== 'NHL');
    }
    fetchNews();
  });

  $("#NBA").change((event) => {
    var checked = event.target.checked;
    if (checked) {
      selectedNewsFeeds.push('NBA');
    } else {
      selectedNewsFeeds = selectedNewsFeeds.filter(feed => feed !== 'NBA');
    }
    fetchNews();
  });

  $("#Favorites").change((event) => {
    var checked = event.target.checked;
    if (checked) {
      globalItems = favorites;
      ["#NBA", "#MLB", "#NHL"].forEach((string) => {
        $(string).prop('checked', false);
      });
      updateHTML();
    } else {
      selectedNewsFeeds.forEach((newsFeed) => {
        $(`#${newsFeed}`).prop('checked', true);
      });
      fetchNews();
    }
  });

}

function loadFavorites() {
  var user = localStorage.getItem('currentUser');
  if (user) {
    var info = JSON.parse(localStorage.getItem(user));
    favorites = info['favorites'] ? info['favorites'] : [];
    $("#login").remove();
    $("#right-nav").append("<a class=\"nav-link\" href=\"#\" id=\"logout\">Logout</a>");
    $("#logout").click(function() {
      logout();
    });
    $("#brand").text(`${user}'s Personal News Feed`);
  } else {
    $("#brand").text(`Personal News Feed`)
  }
}

function saveFavorites() {
  var user = localStorage.getItem('currentUser');
  if (user) {
    var data = JSON.parse(localStorage.getItem(user));
    Object.assign(data, {
      favorites: favorites
    });
    localStorage.setItem(user, JSON.stringify(data));
  }
}

function logout() {
  saveFavorites();
  localStorage.removeItem('currentUser');
  $("#logout").remove();
  $("#right-nav").append("<a class=\"nav-link\" href=\"/login.html\" id=\"login\">Login / Register</a>");
  $("#brand").text("Personal News Feed");
  favorites = [];
  fetchNews();
  ["#NBA", "#MLB", "#NHL"].forEach((string) => {
    $(string).prop('checked', true);
  });
}

function fetchNews() {
  globalItems = [];
  var obs = [];

  selectedNewsFeeds.forEach(feed => {
    obs.push(
      from($.get(url + feed + '/news/').promise())
    );
  });

  if (obs.length <= 0) {
    updateHTML();
  } else {
    forkJoin(obs)
    .subscribe(xmls => {
      parseXMLDocuments(xmls);
      updateHTML();
    });
  }
}

function parseXMLDocuments(xmls) {
  xmls.forEach(xml => {
    var items = xml.querySelectorAll("item");
    for (var i = 0; i < items.length; i++) {
      //get the data out of the item
      var newsItem = parseItem(items[i]);
      globalItems.push(newsItem);
    }
  });
  globalItems.sort(function(a,b){
    return new Date(b.pubDate) - new Date(a.pubDate);
  });
}

function parseItem(item) {
  var newsItem = {};
  ['title', 'description', 'link', 'pubDate', 'image', 'guid'].forEach((selector) => {
    newsItem[selector] = item.querySelector(selector).firstChild.nodeValue;
    newsItem['favorite'] = favorites.find(favorite => favorite.guid === newsItem.guid) !== undefined;
  });

  return newsItem;
}

function updateHTML() {
  var html = "";
  $("#content").fadeOut(200);
  globalItems.forEach(newsItem => {
    var line = `
      <div class="card" style="width:100%; margin-top: 5px; margin-bottom: 5px;">
        <img class="card-img-top" src="${newsItem['image']}">
        <div class="card-body">
          <h4 class="card-title">${newsItem['title']}</h4>
          <div class="row mb-2 mt-3">
            <p align="left" class="mb-3">${newsItem['description']}</p>
            <p align="left">Publication date: ${newsItem['pubDate']}</p>
          </div>
          <div class="row align-items-center">
            <a href="${newsItem['link']}" target="_blank" class="btn btn-primary">See full story</a>
            <button class="btn btn-info ml-3 favorite-button" id="${newsItem['guid']}">Favorite</button>
            <i class="${newsItem['favorite'] ? 'fas' : 'far'} fa-star ml-3"></i>
          </div>
        </div>
      </div>
    `;
    html += line;
  });
  document.querySelector("#content").innerHTML = html;
  $("#content").fadeIn(1000);

  $(".favorite-button").each(function () {
    const id = $(this).attr('id');
    $(this).click(() => {
      var foundItem = globalItems.find(item => item.guid === id);
      var star = $(this).siblings('.fa-star');
      if (!favorites.find(favorite => favorite.guid === foundItem.guid)) {
        favorites.push(foundItem);
        star.removeClass("far").addClass("fas");
      } else {
        favorites = favorites.filter(favorite => favorite.guid !== foundItem.guid);
        star.removeClass("fas").addClass("far");
      }
    });
  });
}
