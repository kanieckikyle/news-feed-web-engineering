
$(document).ready(function () {
  init();
});

function init() {
  $("#login-button").click(function() {
    login();
  });
}

function login() {
  var username = $("#username").val();
  var password = $("#password").val();

  var user = localStorage.getItem(username);

  if (user) {
    var data = JSON.parse(user);
    setUserInfo(username, data);
  } else {
    setUserInfo(username);
  }
  window.location.replace('/index.html');
}

function setUserInfo(username, data = {}) {
  localStorage.setItem('currentUser', username);
  Object.assign(data, {
    last_login: new Date().toLocaleDateString(),
  });
  localStorage.setItem(username, JSON.stringify(data));
}
